const jwt = require('jsonwebtoken');
const secret = 'CourseBookingSystem'; // we have declared a secret word/phrase that will be used as signature/access token for our project.

//why did we create a secret word?
//we created this secret word/access token to verify the request coming from other apps.

//the purpose of this module is that it will hold all the script regarding to  our app's authorization.

//lets create a function that will allow us to authorize a user using the access token
module.exports.createAccessToken = (user) => {
	//we will identify the props/keys of the user that we want to verify
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin//to identify the role to determine the restrictions.
	}
	return jwt.sign(data, secret, {})
} //through the use of sign() we are creating a synchronouse= signature with a default HMAC(hash-based Message Authentication code.)

//synchronous -> at the same time.

//an access token can come in 2 forms?
//-> "opaque" string
//-> JSON web token

//the reason behind creating/utilizing 2 access tokens.
//-> add another layer of security
//-> verify if the access token came from the authorized/proper origin.
//-> pang add ng random factorsa pag generate ng access toke na tayo lang ang may alam.


//lets create a functiuon to verify whether the access token is correct.
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization //authorization is a request header, commonly used for HTTP basic authorization

	//lets create a control structure to describe.identify whether the token will allowed to pass.

	if(typeof token !== "undefined"){
		//if merong value na nakuha sa authorization prop.
		token = token.slice(7, token.length) //the token will be a string data type.
		//the number of characters that will be sliced off the string (token)
		//WHEN the JWT is generated it will include 7 additional characters to include additional security in the access token.
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? res.send(err) : next()
			//next at ginagamit sa middleware, involing the next() it will let the middleware process to the next function
		})
	}else{
		return res.send({ auth: 'failed' })
	}
}

//we created the following functions.
//-> a function to generate an acess token via JWT with our secret word.
//-> a function to verify if the access token came from the correct/proper origin/resource
//now, its time to decode or decrypt the access token since it is still hashed.
module.exports.decode = (token) => {
	//lets create a control structure to determine the response if an access token is captured
	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? null : jwt.decode(token, {
				complete: true
			}).payload //payload is for options.
		})
	}else{
		return null //developers choice to determine the response... null is when a data is properly identified but not given a value.
	}

}