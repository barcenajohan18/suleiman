const express = require('express');
const mongoose = require('mongoose');

const app = express();

const cors = require('cors');

app.use(cors());

// const corsOptions = {
// 	origin: ['https://barcenajohan18.gitlab.io','https://git.heroku.com/infinite-mountain-20516.git'],
// 	optionSuccessStatus: 200
// };



require('dotenv').config();

const port = process.env.PORT;
const connectionString = process.env.DB_CONNECTION_STRING;

mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas!"));

mongoose.connect(connectionString, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	res.send("Successfully hosted")
})

//we need to define the routes
const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');

app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);

app.listen(port || 4000, () => {
	console.log("API is Now ONLINE")
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

// db.once('open', () => console.log("Now connected to MongoDB Atlas Database"));
