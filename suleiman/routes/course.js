const express = require('express')//lets acquire the dependencies needed. 
const router = express.Router() //lets acquire the dependencies needed.
const courseController = require('../controllers/course.js');
const course = require('../models/course')

//lets create a ENDPOINT for our get all function which will allow us to display all of the courses available for enrollment.
router.get('/', (req, res) => {
	courseController.getAll().then(courses => res.send(courses));
	
})

//lets create a route that will allow us to send a request to create a new course.
router.post('/addCourse', (req, res) => { //end point
    courseController.add(req.body).then(result => res.send(result));
})

//task is to create a route to get a single course from the db.
//the endpoint for this route will be a "placeholder parameter"
router.get('/:id', (req, res) => {
        const courseId = req.params.id //to avoid going back to controllers to check
        courseController.get({courseId}).then(course => res.send(course))
})

router.post('/course-exists',(req, res) => {
	//inside the body section lets decribe what will be the procedure upon sending a request in the route
	courseController.courseExists(req.body).then(result => res.send(result));
})
module.exports = router;